import React, { Fragment } from 'react';

let About = props => {

    return(

        <div className="aboutBackgroungImage-container">
           
          <div className="personalInfo-container">

            <div className="profile-image">

                <h3>Eugene La Madrid</h3>

                <img className="avatar"  src="https://st2.depositphotos.com/1734074/11386/v/950/depositphotos_113862774-stock-illustration-vector-businessman-profile-icon-man.jpg"/>

            </div> 

            <div className="profile-title">

              <h1>Full Stack Web Developer</h1>
            </div> 

            <div className="profile-details">

              <p>
              I am an enthusiastic Web developer eager to contribute to team accomplishment through hard work, attention to detail and excellent organizational skills. <br/>
              Multi-faceted software developer knowledgeable in HTML, CSS, and (MERN) Genuine cooperative person and spurred to learn, develop and exceed expectations  <br/>
              in the softwaredevelopment industry..<br/> 
              <br/>
              </p>

            </div>
            
            <div className="contact-links">
              
              <div className="contact-icon">

                  <a href="http://localhost:3000/contacts"><img className="phone-icon"  src="https://www.flaticon.com/premium-icon/icons/svg/327/327882.svg"/></a>
                  <a href="https://www.linkedin.com/in/eugene-la-madrid-a09a2058/"><img className="linkedIn-icon"  src="https://image.flaticon.com/icons/svg/185/185964.svg"/></a>
                  <a href="mailto:eugene.m.lamadrid@gmail.com"><img className="email-icon"  src="https://image.flaticon.com/icons/svg/552/552486.svg"/></a>
                  <a href="https://gitlab.com/eugenelamadrid"><img className="gitlab-icon"  src="https://cdn2.iconfinder.com/data/icons/social-network-round-gloss-shine/512/gitlab.png"/></a>

              </div>
            </div>  

          </div>
              
        </div>
  
      );
}
export default About;