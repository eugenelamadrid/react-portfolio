import React from 'react';

let Skills = props => {

    return(

        <div className="skills-backgroundimage">

            <div className="icons-container">

                <div className="html-img">
                    <img className="skills-img"  src="https://tse3.mm.bing.net/th?id=OIP.IqaFDDJ_pzpcq8kE2vYPHAHaHa&pid=Api&P=0&w=300&h=300"/>
                </div>

                <div className="css-img">
                    <img className="skills-img"  src="http://icons.iconarchive.com/icons/graphics-vibe/developer/256/css-icon.png"/>
                </div>

                <div className="javascript-img">
                    <img className="skills-img"  src="http://www.dewebsite.org/whatis/img_whatis/javascript.png"/>
                </div>

                <div className="reactjs-img">
                    <img className="skills-img"  src="https://cloud.netlifyusercontent.com/assets/344dbf88-fdf9-42bb-adb4-46f01eedd629/064fc70f-5df3-4333-b9d4-f6abe2f946de/react-wp-app8.png"/>
                </div>

                <div className="mongodb-img">
                    <img className="skills-img"  src="https://severalnines.com/sites/default/files/mongodb_tools_2.png"/>
                </div>
            </div>
            
        </div>

    );
}
export default Skills;