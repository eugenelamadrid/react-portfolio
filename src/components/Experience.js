import React from 'react';

let Experience = props => {

    return(

        <div className="experience-backgroundimage">

            <div className="experience-container">        

                <div className="experience-details">
                
                    <h3>Telistar Solutions Pte. Ltd (Singapore)</h3>
                    <h3>Software Support Engineer/Quality Assurance Test Engineer</h3>

                    <h4>2012–2019</h4>
                    
                    <p>
                        •	Review software requirements, specifications and provide timely and meaningful feedback<br/>
                        •	Create detailed, comprehensive and well-structured test cases<br/>
                        •	Liaise with internal teams (e.g. developers and project managers) to identify system requirements<br/>
                        •	Reproducing issues and provide assistance in finding the root cause experienced by clients<br/>

                        •	Taking initiative in building, repairing and maintaining the test rigs<br/>
                        •	Maintaining Petrol System Stability and performance<br/>
                        •	Provide phone, remote and onsite support to 40 Singapore Petroleum site nationwide, SHELL, <br/>
                            CALTEX and TOTAL Petroleum in the ASIA PACIFIC region with regards to NAMOS forecourt system.

                    </p>
                </div>
            </div>

       </div>    


    );
}
export default Experience;