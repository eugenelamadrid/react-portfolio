import React from 'react';
import logo from './logo.svg';
import './App.css';
import About from './components/About';
import Experience from './components/Experience';
import Contacts from './components/Contacts';
import Skills from './components/Skills';


import {
  BrowserRouter, 
  Route, 
  Link,
  NavLink} 
  from 'react-router-dom';
import Login from './components/Login';

class App extends React.Component {

  state = {
   
    isLoggedIn: false,  
    
       
  }


  render(){
  
    return(
      <BrowserRouter>
      <div className="App"></div>
       
        <header>
          <nav className="nav-container">

            <div className="logo">

              <h4>myPortfolio</h4>
              
            </div>
            <ul className="main-nav">

             
              <NavLink to="/"><li><h3>About Me</h3></li></NavLink>
              <NavLink to="/experience"><li><h3>Experience</h3></li></NavLink>
              <NavLink to="/skills"><li><h3>Skills</h3></li></NavLink>
              <NavLink to="/contacts"><li><h3>Contacts</h3></li></NavLink>

            </ul>
                              
          </nav>
        </header>

        {/* <Route path="(/|home)" exact >
          <Home/>
          <div className="login">
              <Login/>
              <button id="continue" onClick={
              ()=> { 
                localStorage.setItem("isLoggedIn",true);
                this.setState({isLoggedIn : true});
              }
            }
            > Continue >>>></button>
            </div>
          

        </Route> */}

        <Route path="(/|about)" exact >
          <About/>
        </Route>

        <Route path="(/experience)" exact >
          <Experience />
        </Route>

        <Route path="(/skills)" exact >
          <Skills />
        </Route>

        <Route path="(/contacts)" exact >
          <Contacts/>
        </Route>
         

          
        
      </BrowserRouter>




    );

  }


}

export default App;
